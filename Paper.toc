\contentsline {chapter}{\numberline {第1章}はじめに}{1}{chapter.1}% 
\contentsline {section}{\numberline {1.1}研究背景}{1}{section.1.1}% 
\contentsline {section}{\numberline {1.2}複数移動ロボットによる協調搬送}{1}{section.1.2}% 
\contentsline {subsection}{\numberline {1.2.1}Grasping}{1}{subsection.1.2.1}% 
\contentsline {subsection}{\numberline {1.2.2}Pushing}{2}{subsection.1.2.2}% 
\contentsline {subsection}{\numberline {1.2.3}Caging}{2}{subsection.1.2.3}% 
\contentsline {section}{\numberline {1.3}マイクロロボット}{3}{section.1.3}% 
\contentsline {section}{\numberline {1.4}関連研究}{3}{section.1.4}% 
\contentsline {section}{\numberline {1.5}研究目的}{4}{section.1.5}% 
\contentsline {section}{\numberline {1.6}論文の構成}{4}{section.1.6}% 
\contentsline {chapter}{\numberline {第2章}光熱マイクロロボット}{7}{chapter.2}% 
\contentsline {section}{\numberline {2.1}ハードウェア構成}{7}{section.2.1}% 
\contentsline {section}{\numberline {2.2}マイクロバブルロボットの動作原理と振る舞い}{8}{section.2.2}% 
\contentsline {section}{\numberline {2.3}まとめ}{9}{section.2.3}% 
\contentsline {chapter}{\numberline {第3章}マイクロバブルロボットの検出と識別}{11}{chapter.3}% 
\contentsline {section}{\numberline {3.1}マイクロバブルロボットの検出アルゴリズム}{11}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}変数初期化}{12}{subsection.3.1.1}% 
\contentsline {subsection}{\numberline {3.1.2}画像読み込み}{12}{subsection.3.1.2}% 
\contentsline {subsection}{\numberline {3.1.3}グレースケール化}{12}{subsection.3.1.3}% 
\contentsline {subsection}{\numberline {3.1.4}二値化}{12}{subsection.3.1.4}% 
\contentsline {subsection}{\numberline {3.1.5}ハフ変換}{12}{subsection.3.1.5}% 
\contentsline {section}{\numberline {3.2}動画処理アルゴリズム}{13}{section.3.2}% 
\contentsline {section}{\numberline {3.3}マイクロバブルロボットの識別とトラッキング}{13}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}欠損値が存在する場合の処理}{14}{subsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.3.2}バブルが増加する動画の処理}{14}{subsection.3.3.2}% 
\contentsline {section}{\numberline {3.4}データのエラー訂正処理}{15}{section.3.4}% 
\contentsline {subsection}{\numberline {3.4.1}画面外へ移動した場合の処理}{15}{subsection.3.4.1}% 
\contentsline {subsection}{\numberline {3.4.2}モーションブラーの除去}{16}{subsection.3.4.2}% 
\contentsline {subsection}{\numberline {3.4.3}フィルタリング後のデータ}{17}{subsection.3.4.3}% 
\contentsline {section}{\numberline {3.5}まとめ}{17}{section.3.5}% 
\contentsline {chapter}{\numberline {第4章}Cagingの定式化と判定\cite {Magariyama} }{19}{chapter.4}% 
\contentsline {section}{\numberline {4.1}C-Closure Object}{19}{section.4.1}% 
\contentsline {section}{\numberline {4.2}Object Closure}{20}{section.4.2}% 
\contentsline {section}{\numberline {4.3}CC-Closure ObjectとObject Closrueの十分条件判}{21}{section.4.3}% 
\contentsline {section}{\numberline {4.4}まとめ}{23}{section.4.4}% 
\contentsline {chapter}{\numberline {第5章}ボケの影響によるバブル直径計測の信頼性評価}{25}{chapter.5}% 
\contentsline {section}{\numberline {5.1}ピントのズレによって生ずる計測値の変動}{25}{section.5.1}% 
\contentsline {subsection}{\numberline {5.1.1}実験動画}{25}{subsection.5.1.1}% 
\contentsline {subsection}{\numberline {5.1.2}解析}{26}{subsection.5.1.2}% 
\contentsline {subsection}{\numberline {5.1.3}結論}{26}{subsection.5.1.3}% 
\contentsline {section}{\numberline {5.2}画像のボケが存在する場合の直径計測手法の妥当性の検証}{27}{section.5.2}% 
\contentsline {subsubsection}{閾値の手法}{27}{section*.30}% 
\contentsline {subsection}{\numberline {5.2.1}解析結果}{27}{subsection.5.2.1}% 
\contentsline {subsection}{\numberline {5.2.2}結論}{28}{subsection.5.2.2}% 
\contentsline {section}{\numberline {5.3}まとめ}{28}{section.5.3}% 
\contentsline {chapter}{\numberline {第6章}バブル直径の変化の特性解析と予測}{29}{chapter.6}% 
\contentsline {section}{\numberline {6.1}バブル直径の変化特性の解析}{29}{section.6.1}% 
\contentsline {subsection}{\numberline {6.1.1}バブルの直径変化傾向を基にした変化予測}{29}{subsection.6.1.1}% 
\contentsline {subsection}{\numberline {6.1.2}関数モデルの評価}{30}{subsection.6.1.2}% 
\contentsline {section}{\numberline {6.2}予測の信頼性評価}{30}{section.6.2}% 
\contentsline {subsection}{\numberline {6.2.1}標本化期間と予測信頼性の関係評価}{31}{subsection.6.2.1}% 
\contentsline {subsection}{\numberline {6.2.2}標本化期間による予測精度の変化}{32}{subsection.6.2.2}% 
\contentsline {subsection}{\numberline {6.2.3}結論}{32}{subsection.6.2.3}% 
\contentsline {section}{\numberline {6.3}まとめ}{32}{section.6.3}% 
\contentsline {chapter}{\numberline {第7章}マイクロバブルロボットにおける直径の変化特性}{35}{chapter.7}% 
\contentsline {section}{\numberline {7.1}実験動画}{35}{section.7.1}% 
\contentsline {subsection}{\numberline {7.1.1}複数バブルの手動操作による駆動}{35}{subsection.7.1.1}% 
\contentsline {subsection}{\numberline {7.1.2}単体バブルの手動操作による駆動}{35}{subsection.7.1.2}% 
\contentsline {section}{\numberline {7.2}バブル直径の変化傾向と画像の背景輝度との相関}{36}{section.7.2}% 
\contentsline {subsection}{\numberline {7.2.1}輝度との相関の仮定}{36}{subsection.7.2.1}% 
\contentsline {subsection}{\numberline {7.2.2}解析手法}{37}{subsection.7.2.2}% 
\contentsline {subsection}{\numberline {7.2.3}結果}{37}{subsection.7.2.3}% 
\contentsline {section}{\numberline {7.3}バブル直径の変化傾向とチェンバ上の位置との相関}{37}{section.7.3}% 
\contentsline {subsection}{\numberline {7.3.1}解析手法}{38}{subsection.7.3.1}% 
\contentsline {subsection}{\numberline {7.3.2}結果}{39}{subsection.7.3.2}% 
\contentsline {section}{\numberline {7.4}まとめ}{39}{section.7.4}% 
\contentsline {chapter}{\numberline {第8章}バブルによるCaging formation}{41}{chapter.8}% 
\contentsline {section}{\numberline {8.1}Bubble Formationの分類}{41}{section.8.1}% 
\contentsline {section}{\numberline {8.2}Drop Phase}{42}{section.8.2}% 
\contentsline {section}{\numberline {8.3}Approach Phase}{42}{section.8.3}% 
\contentsline {section}{\numberline {8.4}バブル直径の変化モデル}{42}{section.8.4}% 
\contentsline {subsection}{\numberline {8.4.1}モデル1}{43}{subsection.8.4.1}% 
\contentsline {subsection}{\numberline {8.4.2}モデル2}{43}{subsection.8.4.2}% 
\contentsline {subsection}{\numberline {8.4.3}モデル3}{44}{subsection.8.4.3}% 
\contentsline {subsection}{\numberline {8.4.4}モデル4}{44}{subsection.8.4.4}% 
\contentsline {subsection}{\numberline {8.4.5}モデル5}{45}{subsection.8.4.5}% 
\contentsline {subsection}{\numberline {8.4.6}モデル2から5}{46}{subsection.8.4.6}% 
\contentsline {section}{\numberline {8.5}Drop Phaseにおける経路決定指標}{46}{section.8.5}% 
\contentsline {subsection}{\numberline {8.5.1}経路決定の問題}{47}{subsection.8.5.1}% 
\contentsline {subsection}{\numberline {8.5.2}経路選択の条件}{47}{subsection.8.5.2}% 
\contentsline {section}{\numberline {8.6}Drop Phaseにおける経路選択の指標}{48}{section.8.6}% 
\contentsline {subsection}{\numberline {8.6.1}モデル1を用いた計算検討の結果}{48}{subsection.8.6.1}% 
\contentsline {subsection}{\numberline {8.6.2}Waypoint 及び Drop pointの設定}{48}{subsection.8.6.2}% 
\contentsline {subsection}{\numberline {8.6.3}各経路及び所要時間}{49}{subsection.8.6.3}% 
\contentsline {subsection}{\numberline {8.6.4}解析}{50}{subsection.8.6.4}% 
\contentsline {section}{\numberline {8.7}まとめ}{53}{section.8.7}% 
\contentsline {chapter}{\numberline {第9章}まとめ}{55}{chapter.9}% 
\contentsline {section}{\numberline {9.1}結言}{55}{section.9.1}% 
\contentsline {section}{\numberline {9.2}今後の課題}{56}{section.9.2}% 
\contentsline {chapter}{謝辞}{57}{chapter*.70}% 
\contentsline {chapter}{参考文献}{59}{chapter*.71}% 
\contentsline {chapter}{\numberline {付録A}マイクロバブルロボットによる力制御の提案}{61}{appendix.A}% 
\contentsline {subsection}{\numberline {A.0.1}レーザ照射により発生する像}{61}{subsection.A.0.1}% 
\contentsline {subsection}{\numberline {A.0.2}力制御の可能性}{61}{subsection.A.0.2}% 
\contentsline {chapter}{\numberline {付録B}Object Closure算出の簡略化}{63}{appendix.B}% 
\contentsline {subsection}{\numberline {B.0.1}簡略化手法}{63}{subsection.B.0.1}% 
\contentsline {subsection}{\numberline {B.0.2}簡略化により生ずる幾何学的な計算誤差}{63}{subsection.B.0.2}% 
